﻿using Meteo_Lez1_DefinzioneClassi.Model;
using Meteo_Lez1_DefinzioneClassi.ViewModel.Helpers;
using Meteo_Lez2_View.ViewModel.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text;

namespace Meteo_Lez2_View.ViewModel
{
    class WeatherVM : INotifyPropertyChanged
    {
        private string query;

        public string Query
        {
            get { return query; }
            set { 
                query = value;
                OnPropertyChanged("Query");                 //Invoco alla modifica l'evento PropertyChanged che si ocucperà della sincronizzazione del DataContext
            }
        }

        //Creo una variabile di DataContext per la currentCondition
        private CurrentConditions currentConditions;

        public CurrentConditions CurrentConditions
        {
            get { return currentConditions; }
            set
            {
                currentConditions = value;
                OnPropertyChanged("currentConditions");
            }
        }

        private City selectedCity;

        public City SelectedCity
        {
            get { return selectedCity; }
            set
            {
                selectedCity = value;
                OnPropertyChanged("selectedCity");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;       //Generato automaticamente dall'implementazione

        private void OnPropertyChanged(string propertyName)
        {
            //Se la property Changed esiste allora accedo direttamente al suo argomento definito tramite Poperty Name.
            //Questo deve avvenire praticamente tutte le volte che c'è un metodo Setter
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public SearchCommand SearchCommand { get; set; }

        public ObservableCollection<City> Cities { get; set; }                      //Observable

        //ABILITO IL COSTRUTTORE
        public WeatherVM()
        {
            if(DesignerProperties.GetIsInDesignMode(new System.Windows.DependencyObject()))
            {
                SelectedCity = new City()
                {
                    LocalizedName = "Roma"
                };

                CurrentConditions = new CurrentConditions()
                {
                    WeatherText = "Nuvoloso",
                    Temperature = new Temperature
                    {
                        Metric = new Units
                        {
                            Value = 20
                        }
                    }
                };
            }

            SearchCommand = new SearchCommand(this);        //Trasferisco al Search Command il ViewModel
            Cities = new ObservableCollection<City>();
        }

        //Comando
        public async void MakeQuery()
        {
            var cities = await AccuWearherHelper.GetCities(Query);

            //Cities = new ObservableCollection<City>();                    //Qui la reinizializzo ogni volta e perdo il binding, devo metterla nel costruttore

            Cities.Clear();
            foreach(var city in cities)
            {
                Cities.Add(city);
            }
        }

       
    }
}
