﻿using Meteo_Lez1_DefinzioneClassi.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Meteo_Lez1_DefinzioneClassi.ViewModel.Helpers
{
    class AccuWearherHelper
    {
        //http://dataservice.accuweather.com/locations/v1/cities/autocomplete?apikey=ZwajNdE5B8A136rGIvRG0gbxLphN2azG&q=Pratola
        public const string BASE_URL = "http://dataservice.accuweather.com/";
        public const string AUTOCOMPLETE_ENDPOINT = "locations/v1/cities/autocomplete?apikey={0}&q={1}";
        public const string CORRENTCONDITIONS_ENDPOINT = "currentconditions/v1/{0}?apikey={1}";
        public const string API_KEY = "ZwajNdE5B8A136rGIvRG0gbxLphN2azG";

        //public static async List<City> GetCities(String query)
        //Devo convertire tutto dopo in task perché è il tipo restitute da GEtAsync e ReadAsStringASync

        public static async Task<List<City>> GetCities(String query)
        {
            List<City> cities = new List<City>();

            string url = BASE_URL + String.Format(AUTOCOMPLETE_ENDPOINT, API_KEY, query);

            using (HttpClient client = new HttpClient())
            {
                var response = await client.GetAsync(url);
                string json = await response.Content.ReadAsStringAsync();

                //Per il deserializing dobbiamo installare Newtonsoft Json
                cities = JsonConvert.DeserializeObject<List<City>>(json);
            }

            return cities;
        }

        public static async Task<CurrentConditions> getCurrentConditions(string cityKey)
        {
            CurrentConditions currentConditions = new CurrentConditions();

            string url = BASE_URL + String.Format(CORRENTCONDITIONS_ENDPOINT, cityKey, API_KEY);

            using (HttpClient client = new HttpClient())
            {
                var response = await client.GetAsync(url);
                string json = await response.Content.ReadAsStringAsync();

                //Per il deserializing dobbiamo installare Newtonsoft Json
                currentConditions = (JsonConvert.DeserializeObject<List<CurrentConditions>>(json)).FirstOrDefault();
            }

            return currentConditions;
        }
    }
}
