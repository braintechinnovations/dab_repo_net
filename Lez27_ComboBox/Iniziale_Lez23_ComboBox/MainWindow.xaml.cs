﻿using Iniziale_Lez23_ComboBox.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Iniziale_Lez23_ComboBox
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            List<Studente> elenco = new List<Studente>();

            elenco.Add(new Studente() { Nome = "Giovanni", Cognome = "Pace", PercComple = 25.9f });
            elenco.Add(new Studente() { Nome = "Mario", Cognome = "Rossi", PercComple = 98.0f });
            elenco.Add(new Studente() { Nome = "Valeria", Cognome = "Verdi", PercComple = 67.9f });

            icElencoPerc.ItemsSource = elenco;
        }

        private void icElencoPerc_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox selezione = sender as ComboBox;
            Studente item = (Studente)selezione.SelectedItem;
        }
    }
}
