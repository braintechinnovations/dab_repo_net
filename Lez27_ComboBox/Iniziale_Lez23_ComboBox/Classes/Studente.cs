﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Iniziale_Lez23_ComboBox.Classes
{
    class Studente
    {
        public string Nome { get; set; }
        public string Cognome { get; set; }
        public float PercComple { get; set; }
    }
}
