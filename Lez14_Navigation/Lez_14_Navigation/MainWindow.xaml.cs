﻿using Lez_14_Navigation.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Lez_14_Navigation
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>/// 

    public partial class MainWindow : Window
    {
        private PaginaUno paginaUno = new PaginaUno();
        private PaginaDue paginaDue = new PaginaDue();

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            framePrincipale.Content = paginaUno;
        }
        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            framePrincipale.Content = paginaDue;
        }
    }
}
