﻿using Lez21_Contatti.Classes;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Lez21_Contatti
{
    /// <summary>
    /// Logica di interazione per InserisciUtente.xaml
    /// </summary>
    public partial class InserisciUtente : Window
    {
        public InserisciUtente()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Contatto giovanni = new Contatto()
            {
                nominativo = "Giovanni Pace",
                email = "test@test.com",
                telefono = "12345"
            };

            string dbName = "Rubrica.db";
            string fldPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            string dbPath = System.IO.Path.Combine(fldPath, dbName);

            using (SQLiteConnection conn = new SQLiteConnection(dbPath))
            {
                conn.CreateTable<Contatto>();
                conn.Insert(giovanni);
            }
        }
    }
}
