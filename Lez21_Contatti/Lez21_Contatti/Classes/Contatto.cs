﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lez21_Contatti.Classes
{
    class Contatto
    {
        [PrimaryKey, AutoIncrement]
        public int id { get; set; }

        [MaxLength(250)]
        public string nominativo { get; set; }

        [MaxLength(50)]
        public string telefono { get; set; }

        [Unique]
        public string email { get; set; }
    }
}
