﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lez18_Raggruppamento.Classes
{
    public enum tipoSesso { maschio, femmina }

    class Utente
    {
        private string nome;
        private string email;
        private string eta;

        private tipoSesso sesso;

        public tipoSesso Sesso
        {
            get { return sesso; }
            set { sesso = value; }
        }


        public string Nome
        {
            get { return nome; }
            set { nome = value; }
        }

        public string Eta
        {
            get { return eta; }
            set { eta = value; }
        }

        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        public override string ToString()
        {
            return $"{Nome}, {Eta} - {Email}";
        }

    }
}
