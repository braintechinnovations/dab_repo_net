﻿using Lez18_Raggruppamento.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Lez18_Raggruppamento
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            List<Utente> elenco = new List<Utente>();

            elenco.Add(new Utente() { Nome = "Giovanni", Eta = "35", Email = "giovanni@ciao.com", Sesso=tipoSesso.maschio });
            elenco.Add(new Utente() { Nome = "Mario", Eta = "35", Email = "mario@ciao.com", Sesso = tipoSesso.maschio });
            elenco.Add(new Utente() { Nome = "Maria", Eta = "33", Email = "mari@ciao.com", Sesso = tipoSesso.femmina });
            elenco.Add(new Utente() { Nome = "Valeria", Eta = "33", Email = "valeria@ciao.com", Sesso = tipoSesso.femmina });
            elenco.Add(new Utente() { Nome = "Marco", Eta = "33", Email = "marco@ciao.com", Sesso = tipoSesso.maschio });

            lvTest.ItemsSource = elenco;

            CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(lvTest.ItemsSource);
            PropertyGroupDescription groupDescriptionSesso = new PropertyGroupDescription("Sesso");
            PropertyGroupDescription groupDescriptionEta = new PropertyGroupDescription("Eta");
            view.GroupDescriptions.Add(groupDescriptionSesso);
            view.GroupDescriptions.Add(groupDescriptionEta);


        }
    }
}
