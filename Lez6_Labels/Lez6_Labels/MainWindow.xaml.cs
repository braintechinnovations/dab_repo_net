﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Lez6_Labels
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void txtTestoSelezione_SelectionChanged(object sender, RoutedEventArgs e)
        {
            TextBox tb = sender as TextBox;

            if(tb.SelectionLength > 1)
            {
                //Console.WriteLine(tb.SelectionLength.ToString());
                MessageBox.Show(tb.SelectedText);
            }
        }
    }
}
