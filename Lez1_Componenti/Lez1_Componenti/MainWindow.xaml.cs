﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Lez1_Componenti
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            var variabile = "Giovanni";
            Console.WriteLine(variabile);

            InitializeComponent();

            Button btn = new Button();

            WrapPanel wp = new WrapPanel();

            TextBlock tb_1 = new TextBlock();
            tb_1.Text = "CIAO GIO";
            wp.Children.Add(tb_1);

            TextBlock tb_2 = new TextBlock();
            tb_2.Text = "CIAO MAR";
            wp.Children.Add(tb_2);

            btn.Content = wp;

            pannelloPrincipale.Children.Add(btn);
        }
    }
}
