﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace Meteo_Lez2_View.ViewModel.Commands
{
    class SearchCommand : ICommand
    {
        public WeatherVM VM { get; set; }

        public event EventHandler CanExecuteChanged;

        public SearchCommand(WeatherVM vm)
        {
            VM = vm;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            VM.MakeQuery();
        }
    }
}
