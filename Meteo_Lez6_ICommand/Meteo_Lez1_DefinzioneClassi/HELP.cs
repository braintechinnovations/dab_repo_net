﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Meteo_Lez1_DefinzioneClassi
{
    class HELP
    {
        //Aggiungo il metodo MakeQuery() per l'avvio della ricerca delle Città all'interno del ViewModel

        //public async void MakeQuery()
        //{
        //    var cities = await AccuWearherHelper.GetCities(Query);
        //}

        //Creiamo la cartella Commands per effettuare la creazione dei riferimenti all'interno del ViewModel
        //Per potergli passare l'evento devo necessariamente iniettargli il WeatherVM

        //In questo modo riesco a richiamare il metodo in VM senza il Dispatcher
        //public void Execute(object parameter)
        //{
        //    VM.MakeQuery();
        //}

        //A questo punto per renderlo disponibile alla View devo inizializzarlo e passargli la window di riferimento.

        //Dato che fa parte del mio DataContext allora posso chiamarlo:
        //<Button
        //        Command = "{Binding SearchCommand}"
        //        Margin="0,10" 
        //        Content="Ricerca" />

    }
}
