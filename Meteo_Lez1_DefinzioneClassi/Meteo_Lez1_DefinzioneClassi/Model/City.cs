﻿using System;
using System.Collections.Generic;
using System.Text;

/**
 * Modello la classe in base alla risposta della API di riferimento per AutoComplete
 * Faccio una query ed utilizzo JSON Utils, Generate C# Class from JSON: https://jsonutils.com/
 * https://developer.accuweather.com/accuweather-locations-api/apis/get/locations/v1/cities/autocomplete
 */
namespace Meteo_Lez1_DefinzioneClassi.Model
{
    public class Area
    {
        public string ID { get; set; }
        public string LocalizedName { get; set; }
    }

    public class City
    {
        public int Version { get; set; }
        public string Key { get; set; }
        public string Type { get; set; }
        public int Rank { get; set; }
        public string LocalizedName { get; set; }
        public Area Country { get; set; }                   //Ristruttuto il codice in modo da avere meno classi possibile, dato che le altre due si ripetono in contenuto
        public Area AdministrativeArea { get; set; }
    }
}
