﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Meteo_Lez1_DefinzioneClassi
{
    class HELP
    {
        //Per mettere a disposizione il ViewModel all'interno del mio progetto devo necessariamente aggiungerlo al xmlns

        //Devo creare l'oggetto contenente la variabile di contesto (l'oggetto VM)
        //<Window.Resources>
        //    <vm:WeatherVM x:Key="vm"/>
        //</Window.Resources>

        //Aggiungo alla grid il suo DataSource, in questo modo posso fare il binding di Query (altrimenti non lo vedrei)
            //<Grid DataContext = "{StaticResource vm}" >

        //A questo punto posso sincronizzare la Textbox con il valore del DataContext        
            //<TextBlock Text = "Ricerca una città:" />
            //< TextBox Text="{Binding Query, Mode=TwoWay}" />    <---------

        //Per verificare che tutto funzioni faccio un Binding tra quello che c'è all'interno del footer 
        //<TextBlock Text = "{Binding Query}"           <--------
        //                   Foreground="White"
        //                   FontSize="20"
        //                   Margin="20,0"/>

        //Funziona solo se faccio il focusout per default.




    }
}
