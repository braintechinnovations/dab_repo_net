﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Meteo_Lez2_View.ViewModel
{
    class WeatherVM : INotifyPropertyChanged
    {
        private string query;

        public string Query
        {
            get { return query; }
            set { 
                query = value;
                OnPropertyChanged("Query");                 //Invoco alla modifica l'evento PropertyChanged che si ocucperà della sincronizzazione del DataContext
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;       //Generato automaticamente dall'implementazione

        private void OnPropertyChanged(string propertyName)
        {
            //Se la property Changed esiste allora accedo direttamente al suo argomento definito tramite Poperty Name.
            //Questo deve avvenire praticamente tutte le volte che c'è un metodo Setter
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
