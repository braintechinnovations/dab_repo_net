﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lez17_ListViewTemplate.Classes
{
    class Utente
    {
        private string nome;
        private string email;
        private string eta;

        public string Nome
        {
            get { return nome; }
            set { nome = value; }
        }

        public string Eta
        {
            get { return eta; }
            set { eta = value; }
        }

        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        public override string ToString()
        {
            return $"{Nome}, {Eta} - {Email}";
        }

    }
}
