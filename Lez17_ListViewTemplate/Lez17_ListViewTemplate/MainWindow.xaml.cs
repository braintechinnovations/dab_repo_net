﻿using Lez17_ListViewTemplate.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Lez17_ListViewTemplate
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            List<Utente> elenco = new List<Utente>();

            elenco.Add(new Utente() { Nome = "Giovanni", Eta = "35", Email = "giovanni@ciao.com" });
            elenco.Add(new Utente() { Nome = "Mario", Eta = "34", Email = "mario@ciao.com" });
            elenco.Add(new Utente() { Nome = "Marco", Eta = "33", Email = "marco@ciao.com" });

            lvTest.ItemsSource = elenco;

            CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(lvTest.ItemsSource);
            view.SortDescriptions.Add(new SortDescription("Eta", ListSortDirection.Ascending));
            view.SortDescriptions.Add(new SortDescription("Nome", ListSortDirection.Descending));
        }
    }
}
